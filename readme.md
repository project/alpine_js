## Drupal 💙 Alpine.js

## How to use

This module exclusively uses the [Drupal Library system](https://www.drupal.org/docs/theming-drupal/adding-assets-css-js-to-a-drupal-theme-via-librariesyml#js-properties) to find and process javascript files that are either a [Alpine Plugin](https://alpinejs.dev/advanced/extending) or a script that needs to be loaded before Alpine.start is executed.


You can added globally for your entire theme by setting it as dependency in your `theme.info.yml` file 

```yaml
type: theme
libraries:
  - alpine_js/alpine_js

```

Or if you created a module or a single script that requires javascript by adding an attribute on your library definition.

```yaml
drupal.alpine_plugin:
  version: VERSION
  js:
    js/drupal.itemsjs.js: { attributes: { alpine_js: true } }
  dependencies:
    - alpine_js/alpine_js
```



### Alpine Plugin
If your code is an Alpine.js plugin you need to implement a listener for `alpine:init` event.

```js
const myPlugin = function (Alpine) {
  Alpine.directive('directive',(el) => {
    // The actual code of your directive
  });
}

document.addEventListener('alpine:init', () => {
    window.Alpine.plugin(myPlugin);
})
```


### Changelog

#### 3.0.5
  - Fixed issue with missing licences on Alpine Base libraries after the findAndSplitAlpineLibraries function was run
  - Added AlpineJS plugin Resize to the bundled plugins
#### 3.0.4
  - Removed the double require of drupal/core from composer.json
#### 3.0.3
  - Updated AlpineJS and plugins to 3.14.8
  - The global attachment code was causing some issues being loaded on admin route pages. On new installations it is disabled by default. With an explicit option to load it on a admin page if you need to.
  Instead of enabling AlpineJS globally you can use the two options mentioned in How to use section above.
#### 3.0.2
  - Fixed bug, Global attachment of AlpineJS was ignored.
  - Upgraded AlpineJS and AlpineJS-CSP to 3.13.5
#### 3.0.1
 - Upgraded AlpineJS and AlpineJS-CSP to 3.13.3
 - Added Official Alpine Plugins anchor, collapse, focus, intersect and persist
 - Added option to fully disable loading AlpineJS unless it's required by either `attach_library`, `libraries.yml` in a theme or as `dependencies` on another library
 - Removed the need to add alpine_js/alpine to your library definition if the `attributes: { alpinejs: true}` is added to one of the javascript files on your library
 - Attachment order of Alpine, Plugin and your libary files is `Alpine Plugins -> Custom defined plugins -> AlpineJS or AlpineJS-CSP`
 - Added Links to the Plugin documentation on the settings screen
 - This version might work on Drupal 9.5, but is only tested on Drupal 10.x
#### 3.0.0
 - Tagged first release, we have been using this in production for the 1st quarter of 2023 without any issues.
 - Upgraded AlpineJS and AlpineJS-CSP to 3.12.0
 - Fixed a regression issue in the UI showing a false state that CSP version was enabled if you updated from an older dev release.
 - Dropped support for EOL Drupal version (9.3 and earlier)
 - Updated minimal PHP to 8.0 in composer.json as 7.4 is EOL

