<?php

namespace Drupal\alpine_js;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * A service to post manipulate the attached js files.
 */
class AlpineAssetService {

  /**
   * Alpine_JS configuration.
   */
  protected array $alpineConfig;

  /**
   * Is AlpineJS Found?
   */
  protected bool $alpinejsFound = FALSE;

  /**
   * Libraries that need AlpineJS.
   */
  protected array $librariesNeedsAlpine = [];

  /**
   * Libraries defined by this module
   */
  protected array $alpineBaseLibraries = [];

  protected bool $useAlpineCSPVersion = FALSE;

  /**
   * Global enabled or by dependency enabled?
   */
  protected bool $alpineEnabled = TRUE;

  /**
   * The Libraries that do not need AlpineJS.
   */
  protected array $drupalLibraries = [
    'header' => [],
    'footer' => [],
  ];

  /**
   * The re-sorted AlpineJS libraries.
   */
  protected array $alpineDependencies = [];

  protected LibraryDiscoveryInterface $librariesDiscovery;

  /**
   * Default options for libraries that don't have them.
   */
  protected array $defaultOptions = [
    'browsers' => [],
    'cache' => TRUE,
    'group' => JS_LIBRARY,
    'preprocess' => TRUE,
    'weight' => 0,
  ];

  /**
   * Scope of where to put the AlpineJS libraries, footer or header.
   */
  private string $alpineScope;

  /**
   * Construct a new instance.
   */
  public function __construct(ConfigFactoryInterface $configFactory, LibraryDiscoveryInterface $librariesDiscovery) {
    $this->librariesDiscovery = $librariesDiscovery;
    $this->alpineConfig = $configFactory->get('alpine_js.settings')->get();
    $this->alpineScope = ($this->alpineConfig['footer']) ? 'footer' : 'header';
    $this->useAlpineCSPVersion = $this->alpineConfig['csp'] ?? FALSE;
    $this->alpineEnabled = ($this->themeNeedsAlpine() || $this->alpineConfig['global']) ?? false;
  }

  /**
   * Traverse all the sources and split out the ones that  depend on AlpineJS.
   */
  public function findAndSplitAlpineLibraries(&$sources): void {
    if ($this->alpineEnabled || $this->adminNeedsAlpine()) {
      $this->alpineBaseLibraries = $this->getAlpineBaseLibraries();
      foreach ($sources as $fileName => $definition) {
        if (array_key_exists('attributes', $definition) && array_key_exists('alpinejs', $definition['attributes'])) {
          $definition['scope'] = $this->alpineScope;
          $this->librariesNeedsAlpine[$fileName] = $definition;
          unset($sources[$fileName]);
          $this->alpinejsFound = TRUE;
        }
        else {
          $scope = $definition['scope'];
          $this->drupalLibraries[$scope][$fileName] = $definition;
        }
      }
      if ($this->alpinejsFound || $this->alpineEnabled) {
        $this->addAlpineToLibraries();
        $sources = array_merge_recursive($this->drupalLibraries['header'], $this->drupalLibraries['footer']);
      }
    }
  }

  /**
   * Process if the libraries are global, or not in an admin route,
   * or explicitly enabled for admin,
   */
  protected function adminNeedsAlpine(): bool {
    $isAdmin = (bool) \Drupal::service('router.admin_context')->isAdminRoute();
    $isAdminEnabled = $this->alpineConfig['admin'] ?? FALSE;
    return !$isAdmin || $isAdminEnabled;
  }

  /**
   * Check if the current theme needs AlpineJS
   */
  protected function themeNeedsAlpine(): bool {
    $themeLibraries = \Drupal::theme()->getActiveTheme()->getLibraries();
    $found = false;
    if ($themeLibraries) {
      $found = in_array('alpine_js/alpine_js', $themeLibraries,false);
    }
    return $found;
  }

  /**
   * Get the Base Alpine libraries as defined by this module.
   */
  public function getAlpineBaseLibraries(): array {
    $alpineBaseLibraries = [];
    foreach ($this->librariesDiscovery->getLibrariesByExtension('alpine_js') as $library => $assetsInfo) {
      foreach ($assetsInfo['js'] as $definition) {
        $definition['scope'] = $this->alpineScope;
        $path = $assetInfo['source'] ?? $definition['data'];
        $definition += $this->defaultOptions;
        if ( (!isset($definition['license'])) && (isset($assetsInfo['license']))) {
          $definition['license'] = $assetsInfo['license'];

        }
        $alpineBaseLibraries[$library][$path] = $definition;
      }
    }
    return $alpineBaseLibraries;
  }

  /**
   * Add AlpineJS + dependent libraries to the end of the sources.
   * Order is modules, plugins then alpine-core
   */
  public function addAlpineToLibraries(): void {
    // Filter out the core libraries that are attached via library definitions.
    $alpineCorePlugins = [];
    foreach ($this->alpineBaseLibraries as $plugin => $info) {
      $pluginPath = key($info);
      // If the library or attachment requests the CSP version make sure it's loaded
      if (array_key_exists($pluginPath, $this->librariesNeedsAlpine)) {
        if ($plugin === 'alpine-csp') {
          $this->useAlpineCSPVersion = TRUE;
          $this->alpineEnabled = TRUE;
        }
        elseif ($plugin === 'alpine') {
          $this->alpineEnabled = TRUE;
        }
        else {
          $alpineCorePlugins[$plugin] = $info;
        }
        unset($this->librariesNeedsAlpine[$pluginPath]);
      }
    }

    // Todo create option to load / define 3rd party alpineJS plugins.
    foreach ($this->alpineConfig['plugins'] as $plugin => $enabled) {
      if ($enabled && !array_key_exists($plugin, $alpineCorePlugins)) {
        $alpineCorePlugins[$plugin] = $this->alpineBaseLibraries[$plugin];
      }
    }
    $lastItem = array_key_last($this->drupalLibraries[$this->alpineScope]);
    $weight = $lastItem['weight'] ?? 10;
    // Add core plugins
    foreach ($alpineCorePlugins as $plugin => $info) {
      $this->addToAlpineDependencies($info, $weight);
    }
    // add the other libraries needed.
    $this->addToAlpineDependencies($this->librariesNeedsAlpine, $weight);

    // Add AlpineJS as the very last item in the chain, as it contains the
    // alpine.init call;.
    if (TRUE === (bool) $this->alpineConfig['bridge']) {
      $this->addToAlpineDependencies($this->alpineBaseLibraries['drupalbridge'], $weight);
    }

    if ($this->alpineEnabled) {
      $alpineVersion = ($this->useAlpineCSPVersion) ? 'alpine-csp' : 'alpine';
      $this->addToAlpineDependencies($this->alpineBaseLibraries[$alpineVersion], $weight);

      $this->drupalLibraries[$this->alpineScope] += $this->alpineDependencies;
    }
  }

  /**
   * Add a source that needs AlpineJS to the stack.
   */
  protected function addToAlpineDependencies(array $source, float &$weight): void {
    foreach ($source as $sourcePath => $definition) {
      $weight++;
      $definition['group'] = $this->defaultOptions['group'];
      // Override the preprocess, so the bundler actually want to bundle.
      $definition['preprocess'] = TRUE;
      // Remove the alpineJS attribute.
      unset($definition['attributes']['alpinejs']);
      $definition['weight'] = $weight;
      $this->alpineDependencies[$sourcePath] = $definition;
    }
  }

}
