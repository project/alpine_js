<?php

namespace Drupal\alpine_js\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Configure AlpineJS Library support settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The config name used in this form.
   */
  static protected string $configName = 'alpine_js.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'alpinejs_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['global'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Load AlpineJS on all pages?'),
      '#default_value' => $this->config('alpine_js.settings')
          ->get('global') ?? FALSE,
      '#description' =>
        $this->t('Disable to load on demand as library item<br/>Library names: alpine_js/alpine or alpine_js/alpine-csp'),
    ];
    $form['admin'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Load AlpineJS on admin routes'),
      '#default_value' => $this->config('alpine_js.settings')
          ->get('admin') ?? FALSE,
      '#description' => $this->t('Load AlpineJS on admin routes, this can create possible conflicts with other admin only libraries.'),
    ];

    $form['footer'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Load AlpineJS in footer'),
      '#default_value' => $this->config('alpine_js.settings')
          ->get('footer') ?? TRUE,
      '#description' =>
        $this->t('Disable to load AlpineJS and related modules in the HEAD.'),
    ];
    $form['csp'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use the CSP save version of AlpineJS.'),
      '#default_value' => $this->config('alpine_js.settings')
          ->get('csp') ?? FALSE,
      '#description' =>
        $this->t('For more information see the <a href="https://alpinejs.dev/advanced/csp" target="_blank">documentation</a>.'),
    ];
    $form['bridge'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add Drupal to Alpinejs Bridge (experimental!)'),
      '#default_value' => $this->config('alpine_js.settings')
          ->get('bridge') ?? FALSE,
      '#description' =>
        $this->t('Currently only has the $dbg() magic.'),
    ];
    $form['plugins'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Add official AlpineJS Plugins when adding the library.'),
    ];
    $availablePlugins = $this->getAvailablePlugins();
    $pluginSettings = $this->config('alpine_js.settings')->get('plugins'); // todo figure this out.
    foreach ($availablePlugins as $plugin => $pluginTitle) {
      $form['plugins'][$plugin] = [
        '#type' => 'checkbox',
        '#title' => $pluginTitle,
        '#default_value' => $pluginSettings[$plugin] ?? 0,
        "#description" => $this->t('Library name: alpine_js/@plugin <br/>Read the plugin <a href="https://alpinejs.dev/plugins/@plugin" target="_blank">documentation</a>',
          ['@plugin' => $plugin]),
      ];
    }
    return parent::buildForm($form, $form_state);
  }

  protected function t($string, array $args = [], array $options = ['context' => 'alpine_js']): TranslatableMarkup {
    return new TranslatableMarkup($string, $args, $options, $this->getStringTranslation());
  }

  private function getAvailablePlugins(): array {
    return [
      'anchor' => $this->t('Anchor'),
      'collapse' => $this->t('Collapse'),
      'focus' => $this->t('Focus'),
      'intersect' => $this->t('Intersect'),
      'persist' => $this->t('Persist'),
      'resize' => $this->t('Resize'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(self::$configName);
    $config->set('global', $form_state->getValue('global'))
      ->set('footer', $form_state->getValue('footer'))
      ->set('admin', $form_state->getValue('admin'))
      ->set('bridge', $form_state->getValue('bridge'))
      ->set('csp', $form_state->getValue('csp'));

    $pluginState = [];
    foreach (array_keys($this->getAvailablePlugins()) as $plugin) {
      $pluginState[$plugin] = $form_state->getValue($plugin) ?? 0;
    }
    $config->set('plugins', $pluginState);
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [self::$configName];
  }

}
